<?php
ob_start();
session_start();

if (!isset($_SESSION['username'])) { //if login in session is not set
    header("Location: login.php");
}

$targetSensor = $_GET['sensor'];
$targetLocation = $_GET['location'];
$targetInterval = $_GET['interval'];
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/npm.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>
        <title>Admin Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row" style="background:#000000; margin-right: -70px; margin-left: -70px;">
                <h2 style="color: #FFFFFF">SIT Smart Campus Administrative site - Edit Sensor page</h2>
                <p id="info" align="right"><?php echo 'Logged in as: ', $_SESSION['username'] ?> <a href="logout.php" class="btn btn-danger" style="margin-left: 1%">Logout</a></p>
            </div>
            <div class="row">
                <h1>Edit Sensors</h1>
                <form class="loginForm" method="post">
                    <div class="input-group">
                        <input type="text" id="name" name="name" class="form-control" value="<?php echo $targetSensor; ?>" readonly>
                        <input type="text" id="loca" name="loca" class="form-control" value="<?php echo $targetLocation; ?>">
                        <input type="text" id="interval" name="interval" class="form-control" value="<?php echo $targetInterval; ?>">
                        <input type="submit" id="submit" name="add" class="form-control" value="Update" style="background-color: #00FF00;">
                        <a href="index.php" class="btn btn-danger" style="width: 100%;">Cancel</a>
                    </div>
                </form>
                <?php
                if (isset($_POST['add']) && !empty($_POST['name']) && !empty($_POST['loca']) && !empty($_POST['interval'])) {
                    $name = $_POST['name'];
                    $loca = $_POST['loca'];
                    $interval = $_POST['interval'];
                    header('Refresh: 1; URL = index.php');
                }
                ?>

            </div>
        </div>
    </body>
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyCH27O0GgjCaWCtpyR6670Y00Zrs_ZfNpU",
            authDomain: "smart-campus-crowd-monitoring.firebaseapp.com",
            databaseURL: "https://smart-campus-crowd-monitoring.firebaseio.com",
            projectId: "smart-campus-crowd-monitoring",
            storageBucket: "smart-campus-crowd-monitoring.appspot.com",
            messagingSenderId: "934174985508"
        };

        // Initialize the Firebase
        firebase.initializeApp(config);

        // Get a reference to the database service
        var database = firebase.database();
        function writeData() {
            console.log("hellooo");
            var name = "<?php echo $name ?>";
            var loca = "<?php echo $loca ?>";
            var interval = "<?php echo $interval ?>";
            console.log(name + loca + interval);
            database.ref("Sensors/" + name).set({
                location: loca,
                interval: parseInt(interval)
            });
            console.log("end");
            return true;
        }
        writeData();

    </script>
</html>



