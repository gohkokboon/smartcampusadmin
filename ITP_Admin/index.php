<?php
ob_start();
session_start();

if (!isset($_SESSION['username'])) { //if login in session is not set
    header("Location: login.php");
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/npm.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>
        <title>Admin Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row" style="background:#000000; margin-right: -70px; margin-left: -70px;">
                <h2 style="color: #FFFFFF">SIT Smart Campus Administrative site</h2>
                <p id="info" align="right"><?php echo 'Logged in as: ', $_SESSION['username']?> <a href="logout.php" class="btn btn-danger" style="margin-left: 1%">Logout</a></p>
            </div>
            <div class="row">
                <h1>Sensors</h1>
                <a href="add.php" class="btn btn-success">Add Sensor</a>
                <table id="table" class="table table-striped">
                    <thead>
                        <tr>
                            <th data-field="name">Sensor Name</th>
                            <th data-field="location">Location</th>
                            <th data-field="interval">Interval</th>
                            <th data-field="edit"></th>
                            <th data-field="delete"></th>
                        </tr>
                    </thead> 
                </table>
            </div>
        </div>
    </body>

    <!--Connection to Firebase start here-->
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyCH27O0GgjCaWCtpyR6670Y00Zrs_ZfNpU",
            authDomain: "smart-campus-crowd-monitoring.firebaseapp.com",
            databaseURL: "https://smart-campus-crowd-monitoring.firebaseio.com",
            projectId: "smart-campus-crowd-monitoring",
            storageBucket: "smart-campus-crowd-monitoring.appspot.com",
            messagingSenderId: "934174985508"
        };

        // Initialize the Firebase
        firebase.initializeApp(config);

        // Get a reference to the database service
        var database = firebase.database();

        var $table = $('#table');
        var mydata = [];

        var sensors = firebase.database().ref('Sensors');
        sensors.on('value', function (snapshot) {
            console.log("IN");
            var value;
            snapshot.forEach(function (child) {
                var sensorkey = child.key;
                value = child.val();
                console.log(sensorkey);
                var tempdata = {};
                tempdata["name"] = sensorkey;
                tempdata["location"] = value["location"];
                tempdata["interval"] = value["interval"];
                tempdata["edit"] = '<a href="edit.php?sensor='+sensorkey+'&location='+value["location"]+'&interval='+value["interval"]+'"class="btn btn-warning">Edit</a>';
                tempdata["delete"] = '<a href="delete.php?sensor='+sensorkey+'" class="btn btn-warning">Delete</a>';
                mydata.push(tempdata);
            });
            console.log(mydata.length);
            $(function () {
                $('#table').bootstrapTable({
                    data: mydata
                });
            });
        });

    </script>
</html>

