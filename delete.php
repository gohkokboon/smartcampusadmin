<?php
    $selected_category = $_GET['sensor'];
    echo $selected_category;
    header('Refresh: 2; URL = index.php');
?>

<!--Connection to Firebase start here-->
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyCH27O0GgjCaWCtpyR6670Y00Zrs_ZfNpU",
            authDomain: "smart-campus-crowd-monitoring.firebaseapp.com",
            databaseURL: "https://smart-campus-crowd-monitoring.firebaseio.com",
            projectId: "smart-campus-crowd-monitoring",
            storageBucket: "smart-campus-crowd-monitoring.appspot.com",
            messagingSenderId: "934174985508"
        };
        //Determine which sensor
        var sensor = "<?php echo $selected_category ?>"; 
        // Initialize the Firebase
        firebase.initializeApp(config);

        // Get a reference to the database service
        var database = firebase.database();
        var sensors = firebase.database().ref('Sensors/'+sensor);
        sensors.remove();
    </script>

