<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
   ob_start();
   session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/npm.js" type="text/javascript"></script>
        <title>Admin Login Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <div class="search-box">
                        <div class="caption">
                            <h3>Smart Campus Admin Login</h3>
                        </div>
                        
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" class="loginForm" method="post">
                            <div class="input-group">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                                <input type="submit" id="submit" name="login" class="form-control" value="Login" >
                            </div>
                        </form>
                        <?php
                            if (isset($_POST['login']) && !empty($_POST['username']) && !empty($_POST['password'])) {
                                if ($_POST['username'] == 'admin' && $_POST['password'] == 'password') {
                                    $_SESSION['valid'] = true;
                                    $_SESSION['timeout'] = time();
                                    $_SESSION['username'] = 'admin';
                                    echo 'You have entered valid use name and password';
                                    header('Location: index.php');
                                }else {
                                    echo 'Wrong username or password';
                                }
                            }
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
